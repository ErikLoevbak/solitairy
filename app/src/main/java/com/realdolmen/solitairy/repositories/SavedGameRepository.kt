package com.realdolmen.solitairy.repositories

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.solitairy.domain.GameObject

class SavedGameRepository {
    var db = FirebaseFirestore.getInstance()
    var userID = FirebaseAuth.getInstance().currentUser!!.uid
    fun save(gameObject: GameObject) {
        db.collection("/users/$userID/games/").document("gameObject").set(gameObject)
    }
}