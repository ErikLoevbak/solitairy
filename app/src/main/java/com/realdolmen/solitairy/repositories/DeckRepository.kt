package com.realdolmen.solitairy.repositories

import com.realdolmen.solitairy.domain.Deck
import com.realdolmen.solitairy.domain.Draw
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface DeckRepository {
    @GET("new/shuffle/?deck_count=1")
    fun getNewDeck(): Call<Deck>

    @GET("{deckId}/draw/?count=52")
    fun drawAllCards(@Path("deckId") deckId: String): Call<Draw>

}