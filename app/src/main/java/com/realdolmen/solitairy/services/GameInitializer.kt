package com.realdolmen.solitairy.services

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.realdolmen.solitairy.activities.Game
import com.realdolmen.solitairy.activities.LoadingScreen
import com.realdolmen.solitairy.activities.MainActivity
import com.realdolmen.solitairy.domain.Deck
import com.realdolmen.solitairy.domain.Draw
import com.realdolmen.solitairy.domain.GameObject
import com.realdolmen.solitairy.domain.gamecomponents.Card
import com.realdolmen.solitairy.domain.gamecomponents.DataCard
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.CLUBSID
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.DIAMONDSID
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.HEARTSID
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.SPADESID
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.convertCardValue
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.convertSuit
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.createBoard
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.setListIds
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.setLocalImage
import com.realdolmen.solitairy.services.GameConversionHelper.Companion.setVisibleCards
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GameInitializer(context: Context) {

    private lateinit var deckService: DeckService
    var baseContext = context
    var activity = baseContext as LoadingScreen
    var failed = false

    fun startNewGame() {
        getNewDeck()
        getNewDeck()
    }

    private fun getNewDeck() {
        deckService = DeckService()
        var deckCall = deckService.getNewDeck()
        activity.setStatusText("Finding a deck...")

        deckCall.enqueue(object : Callback<Deck> {

            override fun onResponse(call: Call<Deck>, response: Response<Deck>) {
                if (response.code() == 200) {
                    var deckId = response.body()!!.deckId
                    var drawCall = deckService.drawAllCards(deckId)
                    activity.setStatusText("Setting up cards...")
                    println("found deck")

                    drawCall.enqueue(object : Callback<Draw> {

                        override fun onResponse(call: Call<Draw>, response: Response<Draw>) {
                            if (response.code() == 200) {
                                var newGame = convertToGame(response.body()!!)
                                setupGame(newGame)
                            } else{
                                var intent = Intent(baseContext, MainActivity::class.java)
                                Toast.makeText(
                                    baseContext,
                                    "Something went wrong setting up the deck!",
                                    Toast.LENGTH_LONG
                                ).show()
                                baseContext.startActivity(intent)
                            }
                        }

                        override fun onFailure(call: Call<Draw>, t: Throwable) {
                            if (!failed) {
                                failed = true
                                var intent = Intent(baseContext, MainActivity::class.java)
                                Toast.makeText(baseContext, "The game could not be set up properly.", Toast.LENGTH_LONG)
                                    .show()
                                Toast.makeText(
                                    baseContext,
                                    "Check your internet connection and try again.",
                                    Toast.LENGTH_LONG
                                ).show()
                                baseContext.startActivity(intent)
                            }
                        }

                    })

                }
                else{
                    var intent = Intent(baseContext, MainActivity::class.java)
                    Toast.makeText(
                        baseContext,
                        "Something went wrong setting up the deck!",
                        Toast.LENGTH_LONG
                    ).show()
                    baseContext.startActivity(intent)
                }
            }

            override fun onFailure(call: Call<Deck>, t: Throwable) {
                if (!failed) {
                    failed = true
                    var intent = Intent(baseContext, MainActivity::class.java)
                    Toast.makeText(baseContext, "The game could not be set up properly.", Toast.LENGTH_LONG).show()
                    Toast.makeText(baseContext, "Check your internet connection and try again.", Toast.LENGTH_LONG)
                        .show()
                    baseContext.startActivity(intent)
                }
            }

        })
    }

    private fun convertToGame(draw: Draw): GameObject {
        var savedGame = GameObject()
        savedGame.deckId = draw.deckId
        savedGame.remaining = draw.remaining
        var serializableCards = arrayListOf<Card>()
        for (dataCard: DataCard in draw.cards) {
            var card = Card()
            card.code = dataCard.code
            card.image = setLocalImage(dataCard)
            card.suit = convertSuit(dataCard.suit)
            card.value = convertCardValue(dataCard.value)
            setLocalImage(dataCard)
            serializableCards.add(card)
            when (card.suit) {
                SPADESID -> card.color = false
                CLUBSID -> card.color = false
                HEARTSID -> card.color = true
                DIAMONDSID -> card.color = true
            }
        }
        savedGame.time = 0
        savedGame.board = createBoard(serializableCards)
        setVisibleCards(savedGame)
        setListIds(savedGame)
        return savedGame
    }

    private fun setupGame(gameObject: GameObject) {
        var intent = Intent(baseContext, Game::class.java)
        intent.putExtra("game", gameObject)
        baseContext.startActivity(intent)
    }
}