package com.realdolmen.solitairy.services

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.solitairy.activities.Game
import com.realdolmen.solitairy.repositories.SavedGameRepository

class SaveGameService(context: Context) {
    val context = context
    val game = context as Game

    fun saveGame() {
        val savedGame = game.gameObject
        savedGame.board!!.tableau!!.row1 = game.lists[game.TABLEAUROW1ID]
        savedGame.board!!.tableau!!.row2 = game.lists[game.TABLEAUROW2ID]
        savedGame.board!!.tableau!!.row3 = game.lists[game.TABLEAUROW3ID]
        savedGame.board!!.tableau!!.row4 = game.lists[game.TABLEAUROW4ID]
        savedGame.board!!.tableau!!.row5 = game.lists[game.TABLEAUROW5ID]
        savedGame.board!!.tableau!!.row6 = game.lists[game.TABLEAUROW6ID]
        savedGame.board!!.tableau!!.row7 = game.lists[game.TABLEAUROW7ID]
        savedGame.time = game.time

        var repo = SavedGameRepository()
        repo.save(savedGame)
        var db = FirebaseFirestore.getInstance()
        var userID = FirebaseAuth.getInstance().currentUser!!.uid
        db.collection("/users/$userID/games/").document("gameObject").set(savedGame)

    }
}