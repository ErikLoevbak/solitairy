package com.realdolmen.solitairy.services

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.solitairy.activities.Game
import com.realdolmen.solitairy.activities.MainActivity
import com.realdolmen.solitairy.domain.GameObject
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.deviceIsOnline

class GameResumer(val context: Context) {

    fun resumeGame() {
        if (deviceIsOnline(context)) {
            var auth = FirebaseAuth.getInstance()
            var userID = auth.currentUser!!.uid
            val db = FirebaseFirestore.getInstance()
            db.document("/users/$userID/games/gameObject").get().addOnSuccessListener { document ->
                val savedGame = document.toObject(GameObject::class.java)
                var intent = Intent(context, Game::class.java)
                intent.putExtra("game", savedGame)
                context.startActivity(intent)

            }
        } else {
            Toast.makeText(context, "The game could not be set up properly.", Toast.LENGTH_LONG).show()
            Toast.makeText(context, "Check your internet connection and try again.", Toast.LENGTH_LONG).show()
            var intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }
}