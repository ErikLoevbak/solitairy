package com.realdolmen.solitairy.services

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast

class ConnectionHelper {
    companion object {
        private lateinit var cm: ConnectivityManager
        private var activeNetwork: NetworkInfo? = null
        private var isConnected: Boolean = false

        fun alertIfOffline(context: Context) {
            cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            activeNetwork = cm.activeNetworkInfo
            isConnected = activeNetwork?.isConnected == true
            if (!isConnected) {
                Toast.makeText(context, "Your device is offline. Some features may be unavailable.", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        fun deviceIsOnline(context: Context): Boolean {
            cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            activeNetwork = cm.activeNetworkInfo
            isConnected = activeNetwork?.isConnected == true
            return isConnected
        }
    }
}