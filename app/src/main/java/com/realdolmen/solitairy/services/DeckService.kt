package com.realdolmen.solitairy.services

import com.google.gson.GsonBuilder
import com.realdolmen.solitairy.domain.Deck
import com.realdolmen.solitairy.domain.Draw
import com.realdolmen.solitairy.repositories.DeckRepository
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class DeckService {

    companion object {

        var deckRepo: DeckRepository = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://deckofcardsapi.com/api/deck/")
            .build().create(DeckRepository::class.java)
    }

    fun getNewDeck(): Call<Deck> {
        return deckRepo.getNewDeck()
    }

    fun drawAllCards(deckId: String): Call<Draw> {
        return deckRepo.drawAllCards(deckId)
    }

}