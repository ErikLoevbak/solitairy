package com.realdolmen.solitairy.services

class TimeConverter {
    companion object {
        fun intToHMS(rawTime: Int): String {
            var hours = 0
            var minutes = 0
            var seconds: Int
            var hoursString: String
            var minutesString: String
            var secondsString: String
            var total = rawTime
            while (total > 3600) {
                total -= 3600
                hours++
            }
            while (total > 60) {
                total -= 60
                minutes++
            }

            seconds = total

            hoursString = if (hours < 10) {
                "0$hours:"
            } else {
                "$hours"
            }
            minutesString = if (minutes < 10) {
                "0$minutes:"
            } else {
                "$minutes"
            }
            secondsString = if (seconds < 10) {
                "0$seconds"
            } else {
                "$seconds"
            }
            return "$hoursString$minutesString$secondsString"
        }
    }
}