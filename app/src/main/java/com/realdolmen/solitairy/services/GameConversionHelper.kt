package com.realdolmen.solitairy.services

import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.domain.GameObject
import com.realdolmen.solitairy.domain.gamecomponents.Board
import com.realdolmen.solitairy.domain.gamecomponents.Card
import com.realdolmen.solitairy.domain.gamecomponents.DataCard
import com.realdolmen.solitairy.domain.gamecomponents.Tableau

class GameConversionHelper {
    companion object {
        val TABLEAUROW1ID = 0
        val TABLEAUROW7ID = 6
        val STOCKID = 7
        val TALONID = 8
        val SPADESID = 9
        val HEARTSID = 10
        val CLUBSID = 11
        val DIAMONDSID = 12
        fun setVisibleCards(gameObject: GameObject) {
            gameObject.board!!.tableau!!.row1?.get(gameObject.board!!.tableau!!.row1!!.size - 1)!!.showCard = true
            gameObject.board!!.tableau!!.row2?.get(gameObject.board!!.tableau!!.row2!!.size - 1)!!.showCard = true
            gameObject.board!!.tableau!!.row3?.get(gameObject.board!!.tableau!!.row3!!.size - 1)!!.showCard = true
            gameObject.board!!.tableau!!.row4?.get(gameObject.board!!.tableau!!.row4!!.size - 1)!!.showCard = true
            gameObject.board!!.tableau!!.row5?.get(gameObject.board!!.tableau!!.row5!!.size - 1)!!.showCard = true
            gameObject.board!!.tableau!!.row6?.get(gameObject.board!!.tableau!!.row6!!.size - 1)!!.showCard = true
            gameObject.board!!.tableau!!.row7?.get(gameObject.board!!.tableau!!.row7!!.size - 1)!!.showCard = true
        }

        fun setListIds(gameObject: GameObject) {
            for (card in gameObject.board!!.tableau!!.row1!!) {
                card.listID = 0
            }
            for (card in gameObject.board!!.tableau!!.row2!!) {
                card.listID = 1
            }
            for (card in gameObject.board!!.tableau!!.row3!!) {
                card.listID = 2
            }
            for (card in gameObject.board!!.tableau!!.row4!!) {
                card.listID = 3
            }
            for (card in gameObject.board!!.tableau!!.row5!!) {
                card.listID = 4
            }
            for (card in gameObject.board!!.tableau!!.row6!!) {
                card.listID = 5
            }
            for (card in gameObject.board!!.tableau!!.row7!!) {
                card.listID = 6
            }
            for (card in gameObject.board!!.stock!!) {
                card.listID = 7
            }
        }

        fun createBoard(list: ArrayList<Card>): Board {
            var board = Board()
            var tableauList = list.subList(0, 28)
            board.tableau = Tableau()
            board.tableau!!.row1 = arrayListOf(tableauList[0])
            board.tableau!!.row2 = ArrayList(tableauList.subList(1, 3))
            board.tableau!!.row3 = ArrayList(tableauList.subList(3, 6))
            board.tableau!!.row4 = ArrayList(tableauList.subList(6, 10))
            board.tableau!!.row5 = ArrayList(tableauList.subList(10, 15))
            board.tableau!!.row6 = ArrayList(tableauList.subList(15, 21))
            board.tableau!!.row7 = ArrayList(tableauList.subList(21, 28))
            board.stock = ArrayList(list.subList(28, 52))
            return board

        }

        fun convertSuit(suit: String): Int {
            return when (suit) {
                "SPADES" -> SPADESID
                "HEARTS" -> HEARTSID
                "DIAMONDS" -> DIAMONDSID
                "CLUBS" -> CLUBSID
                else -> 0
            }
        }

        fun convertCardValue(value: String): Int? {
            return when (value) {
                "ACE" -> 1
                "2" -> 2
                "3" -> 3
                "4" -> 4
                "5" -> 5
                "6" -> 6
                "7" -> 7
                "8" -> 8
                "9" -> 9
                "10" -> 10
                "JACK" -> 11
                "QUEEN" -> 12
                "KING" -> 13
                else -> 0
            }
        }

        fun setLocalImage(dataCard: DataCard): Int {
            return when (dataCard.code) {
                "2S" -> R.drawable.c2s
                "2H" -> R.drawable.c2h
                "2C" -> R.drawable.c2c
                "2D" -> R.drawable.c2d
                "3S" -> R.drawable.c3s
                "3H" -> R.drawable.c3h
                "3C" -> R.drawable.c3c
                "3D" -> R.drawable.c3d
                "4S" -> R.drawable.c4s
                "4H" -> R.drawable.c4h
                "4C" -> R.drawable.c4c
                "4D" -> R.drawable.c4d
                "5S" -> R.drawable.c5s
                "5H" -> R.drawable.c5h
                "5C" -> R.drawable.c5c
                "5D" -> R.drawable.c5d
                "6S" -> R.drawable.c6s
                "6H" -> R.drawable.c6h
                "6C" -> R.drawable.c6c
                "6D" -> R.drawable.c6d
                "7S" -> R.drawable.c7s
                "7H" -> R.drawable.c7h
                "7C" -> R.drawable.c7c
                "7D" -> R.drawable.c7d
                "8S" -> R.drawable.c8s
                "8H" -> R.drawable.c8h
                "8C" -> R.drawable.c8c
                "8D" -> R.drawable.c8d
                "9S" -> R.drawable.c9s
                "9H" -> R.drawable.c9h
                "9C" -> R.drawable.c9c
                "9D" -> R.drawable.c9d
                "0S" -> R.drawable.c10s
                "0H" -> R.drawable.c10h
                "0C" -> R.drawable.c10c
                "0D" -> R.drawable.c10d
                "JS" -> R.drawable.cjs
                "JH" -> R.drawable.cjh
                "JC" -> R.drawable.cjc
                "JD" -> R.drawable.cjd
                "QS" -> R.drawable.cqs
                "QH" -> R.drawable.cqh
                "QC" -> R.drawable.cqc
                "QD" -> R.drawable.cqd
                "KS" -> R.drawable.cks
                "KH" -> R.drawable.ckh
                "KC" -> R.drawable.ckc
                "KD" -> R.drawable.ckd
                "AS" -> R.drawable.cas
                "AH" -> R.drawable.cah
                "AC" -> R.drawable.cac
                "AD" -> R.drawable.cad
                else -> 0
            }
        }
    }
}