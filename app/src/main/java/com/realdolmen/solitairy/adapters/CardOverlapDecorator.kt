package com.realdolmen.solitairy.adapters

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class CardOverlapDecorator(private val verticalOverlap: Int, private val horizontalOverlap: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        var itemPosition = parent.getChildAdapterPosition(view)
        if (itemPosition == 0) {
            return
        }
        outRect.set(horizontalOverlap, verticalOverlap, 0, 0)
    }
}