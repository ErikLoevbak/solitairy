package com.realdolmen.solitairy.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.activities.Game
import com.realdolmen.solitairy.domain.gamecomponents.Card
import com.realdolmen.solitairy.domain.gamecomponents.Selection
import com.squareup.picasso.Picasso

class TableauListAdapter(private var list: ArrayList<Card>, private val context: Context) :
    RecyclerView.Adapter<TableauListAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(card: Card, position: Int) {
            var cardImage: ImageView = itemView.findViewById(R.id.cardImage) as ImageView
            if (card.showCard) {
                Picasso.get()
                    .load(card.image!!)
                    .placeholder(R.drawable.cardback)
                    .resize(226, 314)
                    .into(cardImage)
                cardImage.setOnClickListener {
                    var currentGame = context as Game
                    currentGame.handleSelection(Selection(card.listID, position))
                }
                if (card.selected) {
                    cardImage.setBackgroundResource(R.drawable.clearbackredborder)
                } else {
                    cardImage.background = null
                }
            } else {
                Picasso.get()
                    .load(R.drawable.cardback)
                    .resize(226, 314)
                    .into(cardImage)
                cardImage.background = null
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview_tableaurow, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(list[position], position)
    }
}