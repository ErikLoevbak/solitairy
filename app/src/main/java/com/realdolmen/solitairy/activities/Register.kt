package com.realdolmen.solitairy.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.domain.User
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.alertIfOffline
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.deviceIsOnline
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var userName: String
    private lateinit var email: String
    private lateinit var password: String
    private lateinit var passwordConfirm: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        alertIfOffline(this)
        register_registerbtn.setOnClickListener {
            userName = register_username.text.toString()
            email = register_email.text.toString()
            password = register_password.text.toString()
            passwordConfirm = register_passwordconfirm.text.toString()

            register()
        }
    }

    private fun register() {
        if (deviceIsOnline(this)) {
            if (validateInput()) {
                auth = FirebaseAuth.getInstance()
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            auth.currentUser!!.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(userName).build())

                            saveUser()

                            auth.signOut()
                            Toast.makeText(this, "You have successfully registered your account.", Toast.LENGTH_LONG)
                                .show()
                            var intent = Intent(this, Login::class.java)
                            startActivity(intent)
                        } else {
                            register_errormsg.text = "Please make sure the e-mail address ends in a correct domain name, such as '.com'"
                        }
                    }
            }
        } else {
            register_errormsg.text =
                "You are not connected to the internet.\nPlease check your connection and try again."
        }
    }

    private fun saveUser() {
        db = FirebaseFirestore.getInstance()
        var user = User()
        user.id = auth.currentUser!!.uid
        user.displayName = userName
        user.games = mutableListOf()
        db.collection("/users").document(auth.currentUser!!.uid).set(user)
    }

    private fun validateInput(): Boolean {
        if ((userName.trim() == "") || (email.trim() == "") || (password.trim() == "") || (passwordConfirm.trim() == "")) {
            register_errormsg.text = "Please fill in all the fields."
            return false
        }

        if (!(Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
            register_errormsg.text = "Please enter a valid email address."
            return false
        }

        if (password.length < 6) {
            register_errormsg.text = "Your password must be at least 6 characters long."
            return false
        }

        if (password != passwordConfirm) {
            register_errormsg.text = "The passwords do not match!"
            return false
        }

        register_errormsg.text = ""
        return true
    }
}
