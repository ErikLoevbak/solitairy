package com.realdolmen.solitairy.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.alertIfOffline
import com.realdolmen.solitairy.services.TimeConverter.Companion.intToHMS
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var back_pressed: Long = 0
    private var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        highscore_image.bringToFront()
        alertIfOffline(this)
        setSupportActionBar(toolbar)
        auth = FirebaseAuth.getInstance()
        if (auth.currentUser == null) {
            var intent = Intent(this, Login::class.java)
            startActivity(intent)
            auth.signOut()
        }

        newgamebutton.setOnClickListener {
            startNewGame()
        }
        setContinueButtonFunction()
        setHighscoreViews()
    }

    private fun setHighscoreViews() {
        val db = FirebaseFirestore.getInstance()
        db.document("/users/${auth.currentUser!!.uid}").get().addOnSuccessListener { documentSnapshot ->
            var highscore = documentSnapshot["highScore"] as Long?
            if (highscore != null) {
                highscore_value.text = intToHMS(highscore.toInt())
            } else {
                highscore_value.text = "Best Time"
            }
        }
    }

    private fun setContinueButtonFunction() {
        auth = FirebaseAuth.getInstance()
        var userID = auth.currentUser!!.uid
        val db = FirebaseFirestore.getInstance()
        db.document("/users/$userID/games/gameObject").get().addOnSuccessListener { document ->
            if (document.exists()) {
                allowContinue()
            } else {
                continuegamebutton.setOnClickListener {
                    Toast.makeText(this, "You don't have any saved games yet.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun allowContinue() {
        var view = findViewById<View>(R.id.continuegamebutton)
        view.setBackgroundResource(R.drawable.whitebackredborder)
        view.setOnClickListener { continueGame() }

    }

    private fun startNewGame() {
        var intent = Intent(this, LoadingScreen::class.java)
        intent.putExtra("isNewGame", true)
        startActivity(intent)
    }

    private fun continueGame() {
        var intent = Intent(this, LoadingScreen::class.java)
        intent.putExtra("isNewGame", false)
        startActivity(intent)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {

                toast!!.cancel()

                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)

            } else {
                toast = Toast.makeText(baseContext, "Press BACK again to exit", Toast.LENGTH_SHORT)
                toast!!.show()
            }
            back_pressed = System.currentTimeMillis()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_logout -> {
                auth.signOut()
                var intent = Intent(this, Login::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}
