package com.realdolmen.solitairy.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.services.GameInitializer
import com.realdolmen.solitairy.services.GameResumer
import kotlinx.android.synthetic.main.activity_loading_screen.*

class LoadingScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading_screen)
        loading_screen.systemUiVisibility =
            View.SYSTEM_UI_FLAG_IMMERSIVE or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        var isNewGame = intent.getBooleanExtra("isNewGame", true)
        if (isNewGame) {
            var gameInitializer = GameInitializer(this)
            gameInitializer.startNewGame()
        } else {
            val gameContinuer = GameResumer(this)
            gameContinuer.resumeGame()
            setStatusText("Resuming game...")
        }
        hideSystemUI()
        loading_screen.setOnClickListener { hideSystemUI() }
    }

    fun setStatusText(text: String) {
        status_text.text = text
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    override fun onBackPressed() {
    }
}
