package com.realdolmen.solitairy.activities

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.adapters.CardOverlapDecorator
import com.realdolmen.solitairy.adapters.TableauListAdapter
import com.realdolmen.solitairy.adapters.TalonListAdapter
import com.realdolmen.solitairy.domain.GameObject
import com.realdolmen.solitairy.domain.gamecomponents.Card
import com.realdolmen.solitairy.domain.gamecomponents.Selection
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.alertIfOffline
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.deviceIsOnline
import com.realdolmen.solitairy.services.SaveGameService
import com.realdolmen.solitairy.services.TimeConverter.Companion.intToHMS
import kotlinx.android.synthetic.main.activity_game.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.fixedRateTimer


class Game : AppCompatActivity() {
    lateinit var gameObject: GameObject
    private var selection1: Selection? = null
    private var selection2: Selection? = null
    lateinit var lists: ArrayList<ArrayList<Card>>
    lateinit var adapters: ArrayList<TableauListAdapter>
    lateinit var views: ArrayList<View>
    lateinit var placeholders: ArrayList<ImageView>
    private var timeCounter: Timer = Timer()


    private var counting = false

    var time = 0
    val TABLEAUROW1ID = 0
    val TABLEAUROW2ID = 1
    val TABLEAUROW3ID = 2
    val TABLEAUROW4ID = 3
    val TABLEAUROW5ID = 4
    val TABLEAUROW6ID = 5
    val TABLEAUROW7ID = 6
    val STOCKID = 7
    val TALONID = 8
    val SPADESID = 9
    val HEARTSID = 10
    val CLUBSID = 11
    val DIAMONDSID = 12

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        gameObject = intent.getSerializableExtra("game") as GameObject
        gameScreen.systemUiVisibility =
            SYSTEM_UI_FLAG_IMMERSIVE or
                    SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    SYSTEM_UI_FLAG_FULLSCREEN or
                    SYSTEM_UI_FLAG_LAYOUT_STABLE
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser == null) {
            var intent = Intent(this, Login::class.java)
            startActivity(intent)
            auth.signOut()
        }
        hideSystemUI()
        alertIfOffline(this)
        initialLoad()
        setOnClicks()
        var toast = Toast.makeText(this, "Swipe down from the top to access the menu!", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.TOP, 0, 0)
        toast.show()
    }

    @SuppressLint("RestrictedApi")
    private fun showMenuItems() {
        fab_close.visibility = VISIBLE
        fab_home.visibility = VISIBLE
        timer_count.visibility = VISIBLE
        fab_pauseplay.visibility = VISIBLE
    }

    @SuppressLint("RestrictedApi")
    private fun hideMenuItems() {
        fab_close.visibility = INVISIBLE
        fab_home.visibility = INVISIBLE
        timer_count.visibility = INVISIBLE
        fab_pauseplay.visibility = INVISIBLE
    }

    private fun initialLoad() {
        lists = ArrayList()
        lists.add(gameObject.board!!.tableau!!.row1 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.tableau!!.row2 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.tableau!!.row3 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.tableau!!.row4 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.tableau!!.row5 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.tableau!!.row6 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.tableau!!.row7 as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.stock!! as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.talon as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.foundations!!.aces as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.foundations!!.hearts as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.foundations!!.clubs as java.util.ArrayList<Card>)
        lists.add(gameObject.board!!.foundations!!.diamonds as java.util.ArrayList<Card>)

        adapters = ArrayList()
        for (x in 0..6) {
            adapters.add(TableauListAdapter(lists[x], this))
        }

        views = ArrayList()
        views.add(tableau1)
        views.add(tableau2)
        views.add(tableau3)
        views.add(tableau4)
        views.add(tableau5)
        views.add(tableau6)
        views.add(tableau7)
        views.add(stock)
        views.add(talon)
        views.add(spades)
        views.add(hearts)
        views.add(clubs)
        views.add(diamonds)

        placeholders = ArrayList()
        placeholders.add(tableauRow1_placeholder)
        placeholders.add(tableauRow2_placeholder)
        placeholders.add(tableauRow3_placeholder)
        placeholders.add(tableauRow4_placeholder)
        placeholders.add(tableauRow5_placeholder)
        placeholders.add(tableauRow6_placeholder)
        placeholders.add(tableauRow7_placeholder)
        for (x in TABLEAUROW1ID..TABLEAUROW7ID) {
            var overlap = 140 + (lists[x].size - 8)
            var currentView = views[x] as RecyclerView
            currentView.adapter = adapters[x]
            currentView.addItemDecoration(CardOverlapDecorator(-(overlap), 0))
            currentView.layoutManager = LinearLayoutManager(this@Game)
            adapters[x].notifyDataSetChanged()
        }

        var talonRecyclerView = views[TALONID] as RecyclerView
        setTalonDisplay()
        talonRecyclerView.addItemDecoration(CardOverlapDecorator(0, -130))
        talonRecyclerView.layoutManager = LinearLayoutManager(this@Game, LinearLayoutManager.HORIZONTAL, false)

        if (lists[STOCKID].isEmpty()) {
            val stockView = views[STOCKID] as ImageView
            stockView.setImageResource(R.drawable.clearbackblackborder)
        }

        for (x in SPADESID..DIAMONDSID) {
            if (lists[x].size > 0) {
                var view = views[x] as ImageView
                view.setImageResource(lists[x][lists[x].size - 1].image!!)
            }
        }

        time = gameObject.time!!
        timer_count.text = intToHMS(time)
    }

    private fun setOnClicks() {
        setFoundationsOnClick(SPADESID, spades)
        setFoundationsOnClick(HEARTSID, hearts)
        setFoundationsOnClick(CLUBSID, clubs)
        setFoundationsOnClick(DIAMONDSID, diamonds)
        gameScreen.setOnClickListener {
            handleSelection(null)
        }
        stock.setOnClickListener { cycleToTalon() }
        fab_home.setOnClickListener {
            leaveGame()
        }
        fab_close.setOnClickListener {
            hideSystemUI()
            hideMenuItems()
        }
        gameScreen.setOnClickListener {
            handleSelection(null)
        }
        fab_pauseplay.setOnClickListener {
            toggleCounting()
        }

        dragdown_bar.setOnTouchListener(OnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN ->
                    showMenuItems()
            }
            false
        })
        for (x in 0..6) {
            placeholders[x].setOnClickListener { handleSelection(Selection(x, -1)) }
        }
    }

    private fun setFoundationsOnClick(listID: Int, view: View) {
        view.setOnClickListener {
            handleSelection(Selection(listID, lists[listID].size - 1))
            if (lists[listID].size > 0) {
                view.setBackgroundResource(R.drawable.clearbackredborder)
            }

        }
    }

    fun handleSelection(selection: Selection?) {
        if (selection == null) {
            deselectCards()
            resetSelection()
            setTalonDisplay()
        } else {
            if (!counting) {
                startCounting()
            }
            if (selection1 == null) {
                if (selection.index != -1) {
                    select(selection)
                } else {
                    deselectCards()
                }

            } else if (selection2 == null) {
                selection2 = selection
                secondSelection(selection)
            }
        }
    }

    private fun select(selection: Selection) {
        if (selection.index != -1) {
            selection1 = selection
            if (selection.listID == TALONID) {
                getCard(selection).selected = true
                setTalonDisplay()
            } else {
                for (x in selection.index until lists[selection.listID].size) {
                    lists[selection.listID][x].selected = true
                    if (views[selection.listID].javaClass == RecyclerView::class.java) {
                        if (selection.listID <= 6) {
                            adapters[selection.listID].notifyDataSetChanged()
                        }
                    }
                }
            }


        }
        selection2 = null
    }

    private fun secondSelection(selection: Selection) {
        if (selection1!!.listID == selection2!!.listID && selection1!!.index == selection2!!.index) {
            if (selection1!!.listID < SPADESID) {
                deselectCards()
                resetSelection()
                selection1 = selection
                handleSelection(Selection(getCard(selection).suit!!, lists[getCard(selection).suit!!].size - 1))
            } else {
                deselectCards()
                resetSelection()
            }
            return
        }
        if (selection1!!.listID <= TABLEAUROW7ID && selection2!!.listID <= TABLEAUROW7ID) {
            if (selection2!!.index != -1) {
                tableauToTableau(selection)
            } else {
                tableauToEmptyTableau()
            }
            return
        }

        if (selection1!!.listID <= TABLEAUROW7ID && selection2!!.listID >= SPADESID) {
            if (selection2!!.index != -1) {
                tableauToFoundation(selection)
            } else {
                tableauToEmptyFoundation(selection)
            }
            return
        }

        if (selection1!!.listID >= SPADESID && selection2!!.listID <= TABLEAUROW7ID) {
            if (selection2!!.index != -1) {
                foundationToTableau(selection)
            } else {
                foundationToEmptyTableau(selection)
            }
            return
        }

        if (selection1!!.listID == TALONID && selection2!!.listID <= TABLEAUROW7ID) {
            if (selection2!!.index != -1) {
                talonToTableau(selection)
            } else {
                talonToEmptyTableau()
            }
            return
        }

        if (selection1!!.listID == TALONID && selection2!!.listID >= SPADESID) {
            if (selection2!!.index != -1) {
                talonToFoundation(selection)
            } else {
                talonToEmptyFoundation(selection)
            }
            return
        }

        if (selection1!!.listID >= SPADESID && selection2!!.listID >= SPADESID) {
            deselectCards()
            resetSelection()
            return
        }

        if (selection2!!.listID == TALONID) {
            deselectCards()
            resetSelection()
            return
        }

    }

    private fun tableauToTableau(selection: Selection) {
        val cardToMove = getCard(selection1)
        val destinationCard = getCard(selection2)
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if ((cardToMove.color != destinationCard.color) && (cardToMove.value!! + 1 == destinationCard.value)) {
            deselectCards()
            moveCards()
            if (lists[listID1].isNotEmpty()) {
                lists[listID1][lists[listID1].size - 1].showCard = true
            } else {
                placeholders[listID1].visibility = VISIBLE
            }
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
        } else {
            deselectCards()
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
            select(selection)

        }
    }

    private fun tableauToEmptyTableau() {
        val cardToMove = getCard(selection1)
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if ((cardToMove.value == 13)) {
            deselectCards()
            moveCards()
            if (lists[listID1].isNotEmpty()) {
                lists[listID1][lists[listID1].size - 1].showCard = true
            } else {
                placeholders[listID1].visibility = VISIBLE
            }
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            adapters[listID2].notifyDataSetChanged()
            placeholders[listID2].visibility = INVISIBLE
            resetSelection()
        } else {
            deselectCards()
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()

        }
    }

    private fun tableauToFoundation(selection: Selection) {
        val cardToMove = getCard(selection1)
        val destinationCard = getCard(selection2)
        var cardsStack = false
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        val index1 = selection1!!.index
        var requiredValue = destinationCard.value!! + 1
        if (listID2 == cardToMove.suit) {
            cardsStack = true
        }
        if (cardToMove.value != requiredValue) {
            cardsStack = false
        }
        if (lists[listID1].size != index1 + 1) {
            cardsStack = false
        }

        if (cardsStack) {
            deselectCards()
            moveCards()
            if (lists[listID1].isNotEmpty()) {
                lists[listID1][lists[listID1].size - 1].showCard = true
            } else {
                placeholders[listID1].visibility = VISIBLE
            }
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            val foundationView = views[listID2] as ImageView
            foundationView.setImageResource(lists[listID2][lists[listID2].size - 1].image!!)
            foundationView.setBackgroundResource(R.drawable.clearbackblackborder)
            resetSelection()
            checkWinCondition()
        } else {
            deselectCards()
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            resetSelection()
            select(selection)

        }
    }

    private fun tableauToEmptyFoundation(selection: Selection) {
        val cardToMove = getCard(selection1)
        var cardsStack = false
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if (listID2 == cardToMove.suit) {
            cardsStack = true
        }
        if (cardToMove.value != 1) {
            cardsStack = false
        }
        if (lists[listID1].size != selection1!!.index + 1) {
            cardsStack = false
        }
        if (cardsStack) {
            deselectCards()
            moveCards()
            if (lists[listID1].isNotEmpty()) {
                lists[listID1][lists[listID1].size - 1].showCard = true
            } else {
                placeholders[listID1].visibility = VISIBLE
            }
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            val foundationView = views[listID2] as ImageView
            foundationView.setImageResource(lists[listID2][lists[listID2].size - 1].image!!)
            foundationView.setBackgroundResource(R.drawable.clearbackblackborder)
            resetSelection()
            checkWinCondition()
        } else {
            deselectCards()
            adapters[listID1].notifyDataSetChanged()
            resetSelection()
            select(selection)

        }
    }

    private fun foundationToTableau(selection: Selection) {
        val cardToMove = getCard(selection1)
        val destinationCard = getCard(selection2)
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if ((cardToMove.color != destinationCard.color) && (cardToMove.value!! + 1 == destinationCard.value)) {
            deselectCards()
            moveCards()
            val foundationView = views[listID1] as ImageView
            if (lists[listID1].isNotEmpty()) {
                foundationView.setImageResource(lists[listID1][lists[listID1].size - 1].image!!)
            } else {
                foundationView.setImageResource(
                    when (listID1) {
                        SPADESID -> R.drawable.spade
                        HEARTSID -> R.drawable.heart
                        CLUBSID -> R.drawable.club
                        DIAMONDSID -> R.drawable.diamond
                        else -> 0
                    }
                )
                foundationView.setBackgroundResource(R.drawable.clearbackblackborder)
            }
            lists[listID2][lists[listID2].size - 1].selected = false
            makeListsFit()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
        } else {
            deselectCards()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
            select(selection)

        }
    }

    private fun foundationToEmptyTableau(selection: Selection) {
        val cardToMove = getCard(selection1)
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if ((cardToMove.value!! == 13)) {
            deselectCards()
            moveCards()
            val foundationView = views[listID1] as ImageView
            if (lists[listID1].isNotEmpty()) {
                foundationView.setImageResource(lists[listID1][lists[listID1].size - 1].image!!)
            } else {
                foundationView.setImageResource(
                    when (listID1) {
                        SPADESID -> R.drawable.spade
                        HEARTSID -> R.drawable.heart
                        CLUBSID -> R.drawable.club
                        DIAMONDSID -> R.drawable.diamond
                        else -> 0
                    }
                )
                foundationView.setBackgroundResource(R.drawable.clearbackblackborder)
            }
            makeListsFit()
            adapters[listID2].notifyDataSetChanged()
            placeholders[listID2].visibility = INVISIBLE
            resetSelection()
        } else {
            deselectCards()
            makeListsFit()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
            select(selection)

        }
    }

    private fun talonToTableau(selection: Selection) {
        val cardToMove = getCard(selection1)
        val destinationCard = getCard(selection2)
        val listID2 = selection2!!.listID
        if ((cardToMove.color != destinationCard.color) && (cardToMove.value!! + 1 == destinationCard.value)) {
            deselectCards()
            moveCards()
            setTalonDisplay()
            makeListsFit()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
        } else {
            deselectCards()
            setTalonDisplay()
            makeListsFit()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()
            select(selection)

        }
    }

    private fun talonToEmptyTableau() {
        val cardToMove = getCard(selection1)
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if ((cardToMove.value == 13)) {
            deselectCards()
            moveCards()
            setTalonDisplay()
            makeListsFit()
            adapters[listID2].notifyDataSetChanged()
            placeholders[listID2].visibility = INVISIBLE
            resetSelection()
        } else {
            deselectCards()
            setTalonDisplay()
            makeListsFit()
            adapters[listID1].notifyDataSetChanged()
            adapters[listID2].notifyDataSetChanged()
            resetSelection()

        }
    }

    private fun talonToFoundation(selection: Selection) {
        val cardToMove = getCard(selection1)
        val destinationCard = getCard(selection2)
        var cardsStack = false
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        val index1 = selection1!!.index
        var requiredValue = destinationCard.value!! + 1
        if (listID2 == cardToMove.suit) {
            cardsStack = true
        }
        if (cardToMove.value != requiredValue) {
            cardsStack = false
        }
        if (lists[listID1].size != index1 + 1) {
            cardsStack = false
        }

        if (cardsStack) {
            deselectCards()
            moveCards()
            setTalonDisplay()
            val foundationView = views[listID2] as ImageView
            foundationView.setImageResource(lists[listID2][lists[listID2].size - 1].image!!)
            foundationView.setBackgroundResource(R.drawable.clearbackblackborder)
            resetSelection()
            checkWinCondition()
        } else {
            deselectCards()
            setTalonDisplay()
            resetSelection()
            select(selection)

        }
    }

    private fun talonToEmptyFoundation(selection: Selection) {
        val cardToMove = getCard(selection1)
        var cardsStack = false
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        if (listID2 == cardToMove.suit) {
            cardsStack = true
        }
        if (cardToMove.value != 1) {
            cardsStack = false
        }
        if (lists[listID1].size != selection1!!.index + 1) {
            cardsStack = false
        }
        if (cardsStack) {
            deselectCards()
            moveCards()
            setTalonDisplay()
            val foundationView = views[listID2] as ImageView
            foundationView.setImageResource(lists[listID2][lists[listID2].size - 1].image!!)
            foundationView.setBackgroundResource(R.drawable.clearbackblackborder)
            resetSelection()
            checkWinCondition()
        } else {
            deselectCards()
            setTalonDisplay()
            resetSelection()
            select(selection)

        }
    }

    private fun checkWinCondition() {
        if ((lists[SPADESID].size == 13) && (lists[HEARTSID].size == 13) && (lists[CLUBSID].size == 13) && (lists[DIAMONDSID].size == 13)) {
            timeCounter.cancel()
            var recordNotification = ""
            var auth = FirebaseAuth.getInstance()
            var db = FirebaseFirestore.getInstance()
            db.document("/users/${auth.currentUser!!.uid}").get().addOnSuccessListener { documentSnapshot ->
                var currentRecord = documentSnapshot["highScore"] as Long?
                if ((currentRecord == null) || (currentRecord.toInt() > time)) {
                    recordNotification = "\nIt's a new personal best!"
                    db.document("/users/${auth.currentUser!!.uid}").update("highScore", time)
                }
                val saveGameDialogBox = DialogInterface.OnClickListener { _, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            saveGame()
                            var intent = Intent(this, LoadingScreen::class.java)
                            startActivity(intent)
                        }

                        DialogInterface.BUTTON_NEGATIVE -> {
                            saveGame()
                            var intent = Intent(this, MainActivity::class.java)
                            intent.putExtra("isNewGame", true)
                            startActivity(intent)
                        }
                    }
                }
                val saveGameDialogBoxInit = AlertDialog.Builder(this)
                saveGameDialogBoxInit.setMessage("Congratulations, you won!\nYour time was ${intToHMS(time)}.$recordNotification")
                    .setPositiveButton("New Game", saveGameDialogBox)
                    .setNegativeButton("Exit", saveGameDialogBox).show()
            }
        }
    }

    private fun cycleToTalon() {
        if (!counting) {
            startCounting()
        }
        deselectCards()
        resetSelection()
        val cardsToTransfer: ArrayList<Card> = ArrayList()
        val amountToTransfer: Int =
            if (lists[STOCKID].size >= 3) {
                3
            } else {
                lists[STOCKID].size
            }

        if (lists[STOCKID].size > 0) {
            addToTalon(cardsToTransfer, amountToTransfer)
            setTalonDisplay()
        } else {
            resetTalon()
            setTalonDisplay()
        }
    }

    private fun addToTalon(cardsToTransfer: ArrayList<Card>, amountToTransfer: Int) {
        for (x in lists[STOCKID].size - 1 downTo lists[STOCKID].size - amountToTransfer) {
            lists[STOCKID][x].listID = TALONID
            cardsToTransfer.add(lists[STOCKID][x])
        }
        lists[STOCKID].removeAll(cardsToTransfer)
        lists[TALONID].addAll(cardsToTransfer)
        if (lists[STOCKID].isEmpty()) {
            var stockImage = views[STOCKID] as ImageView
            stockImage.setImageResource(R.drawable.clearbackblackborder)
        }
    }

    private fun resetTalon() {
        var listToMove: ArrayList<Card> = ArrayList()
        for (x in lists[TALONID].size - 1 downTo 0) {
            listToMove.add(lists[TALONID][x])
        }
        lists[TALONID].removeAll(listToMove)
        lists[STOCKID].addAll(listToMove)
        stock.setImageResource(R.drawable.cardback)
    }

    private fun setTalonDisplay() {
        var cardsToDisplay: ArrayList<Card> = ArrayList()
        var talonListSize = lists[TALONID].size
        val amountToDisplay = if (talonListSize > 3) {
            3
        } else {
            talonListSize
        }

        if (amountToDisplay != 0) {
            for (x in talonListSize - amountToDisplay until talonListSize) {
                cardsToDisplay.add(lists[TALONID][x])
            }
            for (card in cardsToDisplay) {
                card.showCard = true
            }
            talon.adapter = TalonListAdapter(cardsToDisplay, this@Game, lists[TALONID].size)
            talon.adapter!!.notifyDataSetChanged()

        } else {
            talon.adapter = TalonListAdapter(lists[TALONID], this@Game, lists[TALONID].size)
            talon.adapter!!.notifyDataSetChanged()
        }
    }

    private fun moveCards() {
        val listID1 = selection1!!.listID
        val listID2 = selection2!!.listID
        var index1 = selection1!!.index
        var listToMove: ArrayList<Card> = ArrayList()

        for (x in index1 until lists[listID1].size) {
            listToMove.add(lists[listID1][x])
        }
        lists[listID1].removeAll(listToMove)
        for (card in listToMove) {
            card.listID = listID2
        }
        lists[listID2].addAll(listToMove)
    }

    private fun deselectCards() {
        if (selection1 != null) {
            for (x in selection1!!.index until lists[selection1!!.listID].size) {
                lists[selection1!!.listID][x].selected = false
            }
            if (selection1!!.listID <= TABLEAUROW7ID) {
                adapters[selection1!!.listID].notifyDataSetChanged()
            }
        }

        setTalonDisplay()
        spades.setBackgroundResource(R.drawable.clearbackblackborder)
        hearts.setBackgroundResource(R.drawable.clearbackblackborder)
        clubs.setBackgroundResource(R.drawable.clearbackblackborder)
        diamonds.setBackgroundResource(R.drawable.clearbackblackborder)
    }

    private fun resetSelection() {
        selection1 = null
        selection2 = null
    }

    private fun makeListsFit() {
        var listID1 = selection1!!.listID
        var listID2 = selection2!!.listID
        var listSize1 = lists[listID1].size
        var listSize2 = lists[listID2].size
        if (listID1 <= TABLEAUROW7ID && listID1 != 0) {
            var recyclerView = views[listID1] as RecyclerView
            if (listSize1 < 8) {
                while (recyclerView.itemDecorationCount > 0) {
                    recyclerView.removeItemDecorationAt(0)
                }
                recyclerView.addItemDecoration(CardOverlapDecorator(-140, 0))
            } else {
                while (recyclerView.itemDecorationCount > 0) {
                    recyclerView.removeItemDecorationAt(0)
                }
                var overlap = 140 + (listSize1 - 8)
                recyclerView.addItemDecoration(CardOverlapDecorator(-(overlap), 0))
            }
        }
        if (listID2 <= TABLEAUROW7ID && listID2 != 0) {
            var recyclerView = views[listID2] as RecyclerView
            if (listSize2 < 8) {
                while (recyclerView.itemDecorationCount > 0) {
                    recyclerView.removeItemDecorationAt(0)
                }
                recyclerView.addItemDecoration(CardOverlapDecorator(-140, 0))
            } else {
                while (recyclerView.itemDecorationCount > 0) {
                    recyclerView.removeItemDecorationAt(0)
                }
                var overlap = 140 + (listSize2 - 8)
                recyclerView.addItemDecoration(CardOverlapDecorator(-(overlap), 0))

            }
        }
    }

    private fun getCard(selection: Selection?): Card {
        return lists[selection!!.listID][selection.index]
    }

    private fun leaveGame() {
        stopCounting()
        val exitGameDialogBox = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {

                    val saveGameDialogBox = DialogInterface.OnClickListener { _, which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> {
                                if (deviceIsOnline(this)) {
                                    saveGame()
                                    var intent = Intent(this, MainActivity::class.java)
                                    startActivity(intent)
                                } else {
                                    val exitAnywayDialogBox = DialogInterface.OnClickListener { _, which ->
                                        when (which) {
                                            DialogInterface.BUTTON_POSITIVE -> {
                                                var intent = Intent(this, MainActivity::class.java)
                                                startActivity(intent)
                                            }

                                            DialogInterface.BUTTON_NEGATIVE -> {
                                                hideSystemUI()
                                                hideMenuItems()
                                            }
                                        }
                                    }
                                    val saveGameDialogBoxInit = AlertDialog.Builder(this)
                                    saveGameDialogBoxInit.setMessage("You are not connected to the internet.\nYou cannot save your game right now.")
                                        .setPositiveButton("Exit without saving", exitAnywayDialogBox)
                                        .setNegativeButton("Back to game", exitAnywayDialogBox).show()
                                }
                            }

                            DialogInterface.BUTTON_NEGATIVE -> {
                                var intent = Intent(this, MainActivity::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                    val saveGameDialogBoxInit = AlertDialog.Builder(this)
                    saveGameDialogBoxInit.setMessage("Would you like to save?")
                        .setPositiveButton("Yes", saveGameDialogBox)
                        .setNegativeButton("No", saveGameDialogBox).show()
                }
                DialogInterface.BUTTON_NEGATIVE -> {
                    hideSystemUI()
                    hideMenuItems()
                }
            }
        }

        val exitGameDialogBoxInit = AlertDialog.Builder(this)
        exitGameDialogBoxInit.setMessage("Exit this game?").setPositiveButton("Yes", exitGameDialogBox)
            .setNegativeButton("No", exitGameDialogBox).show()


    }

    private fun saveGame() {
        deselectCards()
        stopCounting()
        var gameSaver = SaveGameService(this)
        gameSaver.saveGame()
    }

    private fun toggleCounting() {
        if (!counting) {
            startCounting()
        } else {
            stopCounting()
        }
    }

    private fun startCounting() {
        fab_pauseplay.setImageResource(R.drawable.pauseicon)
        counting = true
        timeCounter = fixedRateTimer("timer", false, 0, 1000) {
            this@Game.runOnUiThread {
                time++
                timer_count.text = intToHMS(time)
            }
        }
    }

    private fun stopCounting() {
        fab_pauseplay.setImageResource(R.drawable.playicon)
        counting = false
        timeCounter.cancel()
    }

    override fun onBackPressed() {
        hideSystemUI()
        hideMenuItems()
    }

    override fun onPause() {
        stopCounting()
        super.onPause()
    }

    override fun onStop() {
        stopCounting()
        super.onStop()
    }

    override fun onDestroy() {
        stopCounting()
        super.onDestroy()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or SYSTEM_UI_FLAG_FULLSCREEN)
    }

}
