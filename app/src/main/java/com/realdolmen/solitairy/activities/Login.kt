package com.realdolmen.solitairy.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.realdolmen.solitairy.R
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.alertIfOffline
import com.realdolmen.solitairy.services.ConnectionHelper.Companion.deviceIsOnline
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    private var back_pressed: Long = 0
    private var toast: Toast? = null
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        alertIfOffline(this)
        if (auth.currentUser != null) {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        setContentView(R.layout.activity_login)

        login_registerbtn.setOnClickListener {
            val intent = Intent(this, Register::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
        }

        login_loginbtn.setOnClickListener {
            login()
        }
    }

    private fun login() {

        if (deviceIsOnline(this)) {
            var email = login_email.text.toString()
            var password = login_password.text.toString()
            if (email.trim() == "" || password.trim() == "") {
                login_errmsg.text = "Incorrect username and password combination."
            } else {
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            login_errmsg.text = ""
                            val user = auth.currentUser
                            Toast.makeText(this, "Welcome, ${user!!.displayName}!", Toast.LENGTH_LONG).show()
                            var intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        } else {
                            login_errmsg.text = "Incorrect username and password combination."
                        }
                    }

            }
        } else {
            login_errmsg.text = "You are not connected to the internet!"
        }
    }

    override fun onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {

            toast!!.cancel()

            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)

        } else {
            toast = Toast.makeText(baseContext, "Press BACK again to exit", Toast.LENGTH_SHORT)
            toast!!.show()
        }
        back_pressed = System.currentTimeMillis()
    }

    override fun onStart() {
        super.onStart()
        auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null) {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
