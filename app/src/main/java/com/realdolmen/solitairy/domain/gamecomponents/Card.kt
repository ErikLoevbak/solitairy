package com.realdolmen.solitairy.domain.gamecomponents

import java.io.Serializable

class Card : Serializable {
    var code: String? = null
    var image: Int? = null
    var suit: Int? = null
    var value: Int? = null
    var showCard: Boolean = false
    var selected: Boolean = false
    //true represents Red cards, false represents black
    var color: Boolean = false
    var listID: Int = 0
}