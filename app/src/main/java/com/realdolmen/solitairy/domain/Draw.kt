package com.realdolmen.solitairy.domain


import com.google.gson.annotations.SerializedName
import com.realdolmen.solitairy.domain.gamecomponents.DataCard

data class Draw(
    @SerializedName("cards")
    val cards: List<DataCard>,
    @SerializedName("deck_id")
    val deckId: String,
    @SerializedName("remaining")
    val remaining: Int,
    @SerializedName("success")
    val success: Boolean
)