package com.realdolmen.solitairy.domain.gamecomponents

import java.io.Serializable

class Foundations : Serializable {
    var aces: List<Card>? = mutableListOf()
    var clubs: List<Card>? = mutableListOf()
    var diamonds: List<Card>? = mutableListOf()
    var hearts: List<Card>? = mutableListOf()

}
