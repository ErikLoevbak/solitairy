package com.realdolmen.solitairy.domain

import com.realdolmen.solitairy.domain.gamecomponents.Board
import java.io.Serializable

class GameObject : Serializable {
    var id: String? = null
    var deckId: String? = null
    var board: Board? = null
    var remaining: Int? = null
    var time: Int? = null

}
