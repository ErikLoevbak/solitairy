package com.realdolmen.solitairy.domain.gamecomponents

import java.io.Serializable

class Board : Serializable {
    var tableau: Tableau? = Tableau()
    var foundations: Foundations? = Foundations()
    var stock: List<Card>? = null
    var talon: List<Card>? = ArrayList()

}
