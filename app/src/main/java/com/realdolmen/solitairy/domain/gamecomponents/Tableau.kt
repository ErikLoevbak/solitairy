package com.realdolmen.solitairy.domain.gamecomponents

import java.io.Serializable

class Tableau : Serializable {
    var row1: List<Card>? = null
    var row2: List<Card>? = null
    var row3: List<Card>? = null
    var row4: List<Card>? = null
    var row5: List<Card>? = null
    var row6: List<Card>? = null
    var row7: List<Card>? = null
}
