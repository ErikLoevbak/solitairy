package com.realdolmen.solitairy.domain.gamecomponents

class Selection(listID: Int, index: Int) {
    var listID = listID
    var index = index
}