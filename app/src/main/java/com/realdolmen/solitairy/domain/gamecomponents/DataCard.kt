package com.realdolmen.solitairy.domain.gamecomponents


import com.google.gson.annotations.SerializedName

data class DataCard(
    @SerializedName("code")
    val code: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("suit")
    val suit: String,
    @SerializedName("value")
    val value: String
)