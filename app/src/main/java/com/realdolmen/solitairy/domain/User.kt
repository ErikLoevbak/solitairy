package com.realdolmen.solitairy.domain

class User {
    var id: String? = null
    var displayName: String? = null
    var games: List<GameObject>? = null
    var highScore: Int? = null


}